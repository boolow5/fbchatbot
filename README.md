# README #

This is my chatbot for iWeydi news collector.

### What is this repository for? ###

* This chatbot will talk in Somali and will answer basic questions about what happened today
* The users ask 'how is today?', 'what happened yesterday?', 
  or give order to the chat bot such as 'give me', 'read for me' or just 'read' followed by the website name or url


### How do I get set up? ###

* Written in Golang, just to compile you need to run 'go build' inside the root directory
* Will save questions of users and ask later to other users, to save their answer.
* This project has no dependence it's pure Go
* For now this project users Sqlite, which needs no configuration
* The tests can be run by 'go test'
* There is deploy.sh file in the root that will automatically do the deployment

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Author: Mahdi Bollow 
  twitter: @Mahdibolow
  facebook: @boolow5