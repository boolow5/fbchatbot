'use strict'

const express = require('express')
const bodyParser = require('body-parser')
const request = require('request')

const app = express()

app.set('port', (process.env.PORT || 5000))

app.use(bodyParser.urlencoded({extended: false}))

app.get('/', function(req, res) {
  res.send("Hi, I'm FBChatbot")
})

var token = "EAADYXCsART4BAClH7NjduGcTSZABYmaW9ybFXk4d3GDFcsAzd5mDv0c2XzRSp4ShRv1NOGdiizATq0a7FZBqaDGB46jnA6BUGr44s5HV3oRZBY3QfZBClE8rr1tVw7odxaug9blvLKkAyZAL8rqbS5v8aEhkShkRzSiuqZBhDvuAZDZD"

app.get('/webhook/', function(req, res) {
  if (req.query['hub.verify_token'] === "munosharaf") {
    res.send(req.query['hub.challenge'])
  }
  res.send("Wrong token")
})

app.post('/webhook/', function(req, res) {
  console.log("entry:");
  console.log(req.body.entry);
  let messaging_events = req.body.entry[0].messaging
  for (let i=0; i< messaging_events.length; i++) {
    let event = messaging_events[i]
    let sender = event.sender.id
    if (event.message && event.message.text) {
      let text = event.message.text
      sendText(sender, "Text echo:" + text.substring(0,100))
    }
  }
})

function sendText(sender, text) {
  let messageData = {text: text}
  request({
    url: "https://graph.facebook.com/v2.6/me/messages",
    qs: {access_token: token},
    method: "POST",
    json: {
      recipient: {id: sender},
      message: messageData
    }
  }, function(err, response, body) {
    if (err) {
      console.log("sending error:");
      console.log(err);
    } else if (response.body.error) {
      console.log("response body error");
      console.log(response.body.error);
    }
  })
}

app.listen(app.get('port'), function() {
  console.log("running on port "+ app.get('port'));
})
